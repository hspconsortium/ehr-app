import React from 'react';
import hspcLogo from '../../assets/images/hspc-sndbx-logo-wh@2x.png';


const logo = (props) => (
    <div>
        <img src={hspcLogo} alt="HSPC Logo" className="Logo"/>
    </div>
);

export default logo;